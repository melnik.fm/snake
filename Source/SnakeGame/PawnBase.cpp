// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APawnBase::APawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PAwnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	SpawnSnake();
	
}

// Called every frame
void APawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("X_Axis", this, &APawnBase::DirectionX);
	PlayerInputComponent->BindAxis("Y_Axis", this, &APawnBase::DirectionY);
}

void APawnBase::SpawnSnake()
{
	Snake = GetWorld()->SpawnActor<ASnakeBase>(SnakeBaceClass);
}

void APawnBase::DirectionX(float _X)
{
	if (IsValid(Snake))
	{
		if (_X > 0 && Snake->LastDirection != MoveDirection::DOWN)
		{
			Snake->LastDirection = MoveDirection::UP;
		}
		else if (_X < 0 && Snake->LastDirection != MoveDirection::UP)
		{
			Snake->LastDirection = MoveDirection::DOWN;
		}
	}
}

void APawnBase::DirectionY(float _Y)
{
	if (IsValid(Snake))
	{
		if (_Y > 0 && Snake->LastDirection != MoveDirection::RIGHT)
		{
			Snake->LastDirection = MoveDirection::LEFT;
		}
		else if (_Y < 0 && Snake->LastDirection != MoveDirection::LEFT)
		{
			Snake->LastDirection = MoveDirection::RIGHT;
		}
	}
}



