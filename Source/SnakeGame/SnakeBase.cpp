// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60;
	InitialNamberOfElements = 5;
	TimeTick = 0.5;
	LastDirection = MoveDirection::DOWN;
	
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	for (int i = 1; i <= InitialNamberOfElements; i++)
	{
		AddSnakeElement();
	}
	SetActorTickInterval(TimeTick);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement()
{	
		FVector NewVector(ElementSize * ArraySnakeElement.Num(), 0, 0);
		FTransform NewTranform(NewVector);
		ASnakeElementBase* NewElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTranform);
		ArraySnakeElement.Add(NewElement);
}

void ASnakeBase::Move()
{

	FVector Direction(ForceInitToZero);

	switch (LastDirection)
	{
	case MoveDirection::UP:
		Direction.X += ElementSize;
		break;
	case MoveDirection::DOWN:
		Direction.X -= ElementSize;
		break;
	case MoveDirection::LEFT:
		Direction.Y += ElementSize;
		break;
	case MoveDirection::RIGHT:
		Direction.Y -= ElementSize;
		break;
	}

	for (int i = ArraySnakeElement.Num() - 1; i > 0; i--)
	{
		ArraySnakeElement[i]->SetActorLocation(ArraySnakeElement[i - 1]->GetActorLocation());
	}

	ArraySnakeElement[0]->AddActorWorldOffset(Direction);

}

